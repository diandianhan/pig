package com.zs.pig.mall.api.service;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.mall.api.model.TbItemParam;

public interface ItemParamService extends BaseService<TbItemParam>{

	PigResult getItemParamByCid(long cid);
	PigResult insertItemParam(TbItemParam itemParam);
}
