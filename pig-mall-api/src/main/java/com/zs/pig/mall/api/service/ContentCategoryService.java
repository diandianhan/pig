package com.zs.pig.mall.api.service;

import java.util.List;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.common.base.model.EUTreeNode;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.mall.api.model.TbContentCategory;

public interface ContentCategoryService extends BaseService<TbContentCategory>{

	List<EUTreeNode> getCategoryList(long parentId);
	PigResult insertContentCategory(long parentId, String name);
}
