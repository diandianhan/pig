package com.zs.pig.mall.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.common.utils.HttpClientUtil;
import com.zs.pig.mall.api.model.TbContent;
import com.zs.pig.mall.api.service.ContentService;
import com.zs.pig.mall.mapper.TbContentMapper;

/**
 * 内容管理
 * <p>Title: ContentServiceImpl</p>
 * <p>Description: </p>
 * <p>Company: www.itcast.com</p> 
 * @author	入云龙
 * @date	2015年9月8日上午11:09:53
 * @version 1.0
 */
@Service
public class ContentServiceImpl extends
ServiceMybatis<TbContent> implements ContentService {

	@Autowired
	private TbContentMapper  contentMapper;
	@Value("${REST_BASE_URL}")
	private String REST_BASE_URL;
	@Value("${REST_CONTENT_SYNC_URL}")
	private String REST_CONTENT_SYNC_URL;
	@Override
	public PigResult insertContent(TbContent content) {
		//补全pojo内容
				content.setCreated(new Date());
				content.setUpdated(new Date());
				contentMapper.insert(content);
				
				//添加缓存同步逻辑
				try {
					HttpClientUtil.doGet(REST_BASE_URL + REST_CONTENT_SYNC_URL + content.getCategoryId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return PigResult.ok();
	}
	
	
	

}
