package com.zs.pig.mall.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.mall.api.model.TbItemCat;

public interface TbItemCatMapper  extends Mapper<TbItemCat> {
   
}